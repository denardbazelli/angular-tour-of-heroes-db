import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {CarModel} from '../models';

const API_HOST = environment.apiHost;
const CARS_URL = `${API_HOST}/cars`;
const USERS_URL = `${API_HOST}/users`;

@Injectable({
    providedIn: 'root'
})
export class CarService {

    constructor(private http: HttpClient) {
    }

    getUsers() {
        return this.http.get(`${USERS_URL}/`);
    }

    getCars() {
        return this.http.get(`${CARS_URL}/`);
    }

    getCar(id: number) {
        return this.http.get(`${CARS_URL}/${id}/`);
    }

    deleteCar(id: number) {
        return this.http.delete(`${CARS_URL}/${id}/`);
    }

    updateCar(car: CarModel) {
        return this.http.put(`${CARS_URL}/${car.id}/`, car);
    }

    createCar(car: CarModel) {
        return this.http.post(`${CARS_URL}/`, car);
    }


}
