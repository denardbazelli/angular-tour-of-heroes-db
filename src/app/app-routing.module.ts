import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import {CarListComponent} from './car-list/car-list.component';
import {CarDetailComponent} from './car-detail/car-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent,pathMatch: 'full'},
  { path: 'detail/:id', component: HeroDetailComponent, pathMatch: 'full' },
  { path: 'heroes', component: HeroesComponent, pathMatch: 'full' },
  { path: 'cars', component: CarListComponent, pathMatch: 'full' },
  { path: 'cars/form/:id', component: CarDetailComponent, pathMatch: 'full' },
  { path: 'cars/form', component: CarDetailComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
