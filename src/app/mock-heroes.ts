import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 1, name: 'Saitama' },
  { id: 2, name: 'Kakarot' },
  { id: 3, name: 'Ichigo' },
  { id: 4, name: 'Naruto' },
  { id: 5, name: 'Vegeta' },
  { id: 6, name: 'Luffy' },
  { id: 7, name: 'Zorro' },
  { id: 8, name: 'Dr Duritle' },
  { id: 9, name: 'Big Mama' },
  { id: 10, name: 'Kaido' },
  { id: 11, name: 'Dr Nice' },
  { id: 12, name: 'Narco' },
  { id: 13, name: 'Bombasto' },
  { id: 14, name: 'Celeritas' },
  { id: 15, name: 'Magneta' },
  { id: 16, name: 'RubberMan' },
  { id: 17, name: 'Dynama' },
  { id: 18, name: 'Dr IQ' },
  { id: 19, name: 'Magma' },
  { id: 20, name: 'Tornado' }
];
