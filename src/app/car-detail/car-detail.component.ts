import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CarService} from '../_services/car.service';
import {CarModel} from '../models';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatSnackBar} from '@angular/material';
import {getError, setError} from '../error.handler';

@Component({
    selector: 'app-car-detail',
    templateUrl: './car-detail.component.html',
    styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {
    users: any[];
    car = new CarModel();
    carForm: FormGroup;

    constructor(private route: ActivatedRoute,
                private carService: CarService,
                private fb: FormBuilder,
                private router: Router,
                private snackbar: MatSnackBar) {
    }

    ngOnInit() {
        this.getCarDetails();
        this.getUsers();
        this.initialCarForm();
    }

    initialCarForm() {
        this.carForm = this.fb.group({
            'brand': ['', [Validators.required, Validators.maxLength(20)]],
            'model': ['', [Validators.maxLength(20)]],
            'price': ['', [Validators.required, Validators.pattern('^[1-9]\\d*$')]],
            'year_production': ['', [Validators.min(1900), Validators.max(new Date().getFullYear()), Validators.pattern('^[1-9]\\d*$')]],
            'country_of_origin': ['', [Validators.maxLength(20)]],
            'condition': ['', [Validators.required]],
            'seller': [null, [Validators.required]]
        });
    }

    getUsers() {this.carService.getUsers().subscribe(response => this.users = response['results']);}

    getCarDetails() {
        this.car.id = +this.route.snapshot.paramMap.get('id');
        if (this.car.id) {
            this.carService.getCar(this.car.id).subscribe(
                (res: CarModel) => {
                    if (res) {
                        this.carForm.patchValue(res);
                        this.carForm.patchValue({seller: res.seller['id']});
                        console.log(this.carForm);
                    }
                },
                err => console.log(err)
            );
        }
    }

    submit() {
        if (this.carForm.valid) {
            if (this.car.id) {
                let car = new CarModel();
                car = {...this.carForm.value};
                car.id = this.car.id;
                this.carService.updateCar(car).subscribe(
                    () => {
                        this.router.navigate(['/cars']);
                        this.snackbar.open('Updated successfully!', 'OK', {
                            duration: 3000,
                            verticalPosition: 'top',
                            panelClass: 'success-snack'
                        });
                    },
                    error => {
                        this.snackbar.open('Can not update this car!', 'OK', {
                            duration: 3000,
                            verticalPosition: 'top',
                        });
                        this.setError(error.error);
                    }
                );
            } else {
                let car = new CarModel();
                car = {...this.carForm.value};
                this.carService.createCar(car).subscribe(
                    () => {
                        this.router.navigate(['/cars']);
                        this.snackbar.open('Created successfully!', 'OK', {
                            duration: 3000,
                            verticalPosition: 'top',
                            panelClass: 'success-snack'
                        });
                    }, err => {
                        this.snackbar.open('Can not add this car!', 'OK', {
                            duration: 3000,
                            verticalPosition: 'top',
                        });
                        this.setError(err.error);
                    }
                );
            }
        }
    }



    setError(error) {setError(error, this.carForm);}

    getError(field: string): string { return getError(field, this.carForm);}
}
