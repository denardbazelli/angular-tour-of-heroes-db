import {Component, OnInit} from '@angular/core';
import {CarService} from '../_services/car.service';
import {MatDialog} from '@angular/material';
import {DeleteDialogComponent} from '../delete-dialog/delete-dialog.component';


@Component({
    selector: 'app-car-list',
    templateUrl: './car-list.component.html',
    styleUrls: ['./car-list.component.css']
})
export class CarListComponent implements OnInit {
    cars: any[];
    displayedColumns: string[] = ['id', 'brand', 'model', 'year', 'price', 'seller', 'details'];

    constructor(
        private carService: CarService,
        private dialog: MatDialog
    ) {
    }

    ngOnInit() {
        this.getCarList();
    }

    getCarList() {
        this.carService.getCars().subscribe(
            response => this.cars = response['results'],
            error => console.log(error)
        );
    }

    deleteCar(id) {
        const dialogRef = this.dialog.open(DeleteDialogComponent);
        dialogRef.afterClosed().subscribe(
            result => {
                if (result.hasDeleted) {
                    this.carService.deleteCar(id).subscribe(
                        () => this.cars = this.cars.filter(car => car.id !== id),
                        error => console.log('an error has occurred', error)
                    );
                }
            }
        );
    }

}

