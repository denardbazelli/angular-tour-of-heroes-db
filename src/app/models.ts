export class CarModel {
  id: number;
  brand: string;
  model: string;
  year: string;
  price: number;
  seller: number;
}
