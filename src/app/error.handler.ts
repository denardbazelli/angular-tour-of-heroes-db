import {AbstractControl, FormGroup} from '@angular/forms';

let counter: number = 0;

export function setError(error, form: FormGroup) {
    Object.keys(error).forEach(field => {
        const formField: AbstractControl = form.get(field);
        if (formField) {
            formField.setErrors({serverError: {message: error[field][0]}});
        }
    });
}

export function getError(field: string, form: FormGroup): string {
    console.log(counter++);
    const formField: AbstractControl = form.get(field);
    const errors = formField.errors;
    console.log(errors);

    if (errors) {
        if (errors.required) {
            return `fusha eshte e domosdoshme`;
        } else if (errors.maxlength) {
            return `numri i karaktereve nuk mund te kaloje ${errors.maxlength.requiredLength}`;
        } else if (errors.max) {
            return `nuk mund te jete me vone se ${errors.max.max}`;
        } else if (errors.min) {
            return `nuk mund te jete me heret se ${errors.min.min}`;
        } else if (errors.pattern) {
            return `Fusha  nuk eshte e vlefshme`;
        } else if (errors.serverError) {
            return `${errors.serverError.message}`;
        } else {
            return ``;
        }
    }

}


